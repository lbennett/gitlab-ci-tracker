// @flow

import Menubar from 'menubar';
import path from 'path';

const options: Object = {
  dir: path.join(__dirname, '../static'),
};

const menubar: Menubar = Menubar(options);

menubar.on('ready', function () {
  console.log('menubar is ready');
});
