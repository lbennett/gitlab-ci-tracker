'use strict';

var _menubar = require('menubar');

var _menubar2 = _interopRequireDefault(_menubar);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var options = {
  dir: _path2.default.join(__dirname, '../static')
};

var menubar = (0, _menubar2.default)(options);

menubar.on('ready', function () {
  console.log('menubar is ready');
});